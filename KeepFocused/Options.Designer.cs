﻿namespace KeepFocused
{
    partial class Options
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
        	this.TickSoundFileLabel = new System.Windows.Forms.Label();
        	this.AlarmSoundFileLabel = new System.Windows.Forms.Label();
        	this.TickingSoundTextBox = new System.Windows.Forms.TextBox();
        	this.PickTickingSoundFile = new System.Windows.Forms.Button();
        	this.PickAlarmSoundFile = new System.Windows.Forms.Button();
        	this.AlarmSoundTextBox = new System.Windows.Forms.TextBox();
        	this.PlayTickingSoundCheckBox = new System.Windows.Forms.CheckBox();
        	this.PlayAlarmSoundCheckBox = new System.Windows.Forms.CheckBox();
        	this.btnSave = new System.Windows.Forms.Button();
        	this.btnCancel = new System.Windows.Forms.Button();
        	this.SessionTimeLabel = new System.Windows.Forms.Label();
        	this.BreakTimeLabel = new System.Windows.Forms.Label();
        	this.groupBox1 = new System.Windows.Forms.GroupBox();
        	this.groupBox2 = new System.Windows.Forms.GroupBox();
        	this.breakTimeUpDown = new System.Windows.Forms.NumericUpDown();
        	this.sessionTimeUpDown = new System.Windows.Forms.NumericUpDown();
        	this.groupBox3 = new System.Windows.Forms.GroupBox();
        	this.groupBox1.SuspendLayout();
        	this.groupBox2.SuspendLayout();
        	((System.ComponentModel.ISupportInitialize)(this.breakTimeUpDown)).BeginInit();
        	((System.ComponentModel.ISupportInitialize)(this.sessionTimeUpDown)).BeginInit();
        	this.SuspendLayout();
        	// 
        	// TickSoundFileLabel
        	// 
        	this.TickSoundFileLabel.AutoSize = true;
        	this.TickSoundFileLabel.Location = new System.Drawing.Point(12, 24);
        	this.TickSoundFileLabel.Name = "TickSoundFileLabel";
        	this.TickSoundFileLabel.Size = new System.Drawing.Size(98, 13);
        	this.TickSoundFileLabel.TabIndex = 0;
        	this.TickSoundFileLabel.Text = "Ticking Sound File:";
        	// 
        	// AlarmSoundFileLabel
        	// 
        	this.AlarmSoundFileLabel.AutoSize = true;
        	this.AlarmSoundFileLabel.Location = new System.Drawing.Point(21, 65);
        	this.AlarmSoundFileLabel.Name = "AlarmSoundFileLabel";
        	this.AlarmSoundFileLabel.Size = new System.Drawing.Size(89, 13);
        	this.AlarmSoundFileLabel.TabIndex = 1;
        	this.AlarmSoundFileLabel.Text = "Alarm Sound File:";
        	// 
        	// TickingSoundTextBox
        	// 
        	this.TickingSoundTextBox.Location = new System.Drawing.Point(116, 21);
        	this.TickingSoundTextBox.Name = "TickingSoundTextBox";
        	this.TickingSoundTextBox.Size = new System.Drawing.Size(349, 20);
        	this.TickingSoundTextBox.TabIndex = 2;
        	// 
        	// PickTickingSoundFile
        	// 
        	this.PickTickingSoundFile.Location = new System.Drawing.Point(471, 21);
        	this.PickTickingSoundFile.Name = "PickTickingSoundFile";
        	this.PickTickingSoundFile.Size = new System.Drawing.Size(24, 20);
        	this.PickTickingSoundFile.TabIndex = 3;
        	this.PickTickingSoundFile.Text = "...";
        	this.PickTickingSoundFile.UseVisualStyleBackColor = true;
        	this.PickTickingSoundFile.Click += new System.EventHandler(this.PickTickingSoundFile_Click);
        	// 
        	// PickAlarmSoundFile
        	// 
        	this.PickAlarmSoundFile.Location = new System.Drawing.Point(471, 61);
        	this.PickAlarmSoundFile.Name = "PickAlarmSoundFile";
        	this.PickAlarmSoundFile.Size = new System.Drawing.Size(24, 20);
        	this.PickAlarmSoundFile.TabIndex = 4;
        	this.PickAlarmSoundFile.Text = "...";
        	this.PickAlarmSoundFile.UseVisualStyleBackColor = true;
        	this.PickAlarmSoundFile.Click += new System.EventHandler(this.PickAlarmSoundFile_Click);
        	// 
        	// AlarmSoundTextBox
        	// 
        	this.AlarmSoundTextBox.Location = new System.Drawing.Point(116, 61);
        	this.AlarmSoundTextBox.Name = "AlarmSoundTextBox";
        	this.AlarmSoundTextBox.Size = new System.Drawing.Size(349, 20);
        	this.AlarmSoundTextBox.TabIndex = 5;
        	// 
        	// PlayTickingSoundCheckBox
        	// 
        	this.PlayTickingSoundCheckBox.AutoSize = true;
        	this.PlayTickingSoundCheckBox.Location = new System.Drawing.Point(511, 23);
        	this.PlayTickingSoundCheckBox.Name = "PlayTickingSoundCheckBox";
        	this.PlayTickingSoundCheckBox.Size = new System.Drawing.Size(52, 17);
        	this.PlayTickingSoundCheckBox.TabIndex = 6;
        	this.PlayTickingSoundCheckBox.Text = "Play?";
        	this.PlayTickingSoundCheckBox.UseVisualStyleBackColor = true;
        	// 
        	// PlayAlarmSoundCheckBox
        	// 
        	this.PlayAlarmSoundCheckBox.AutoSize = true;
        	this.PlayAlarmSoundCheckBox.Location = new System.Drawing.Point(511, 65);
        	this.PlayAlarmSoundCheckBox.Name = "PlayAlarmSoundCheckBox";
        	this.PlayAlarmSoundCheckBox.Size = new System.Drawing.Size(52, 17);
        	this.PlayAlarmSoundCheckBox.TabIndex = 7;
        	this.PlayAlarmSoundCheckBox.Text = "Play?";
        	this.PlayAlarmSoundCheckBox.UseVisualStyleBackColor = true;
        	// 
        	// btnSave
        	// 
        	this.btnSave.Location = new System.Drawing.Point(520, 352);
        	this.btnSave.Name = "btnSave";
        	this.btnSave.Size = new System.Drawing.Size(75, 23);
        	this.btnSave.TabIndex = 8;
        	this.btnSave.Text = "Save";
        	this.btnSave.UseVisualStyleBackColor = true;
        	this.btnSave.Click += new System.EventHandler(this.SaveButton_Click);
        	// 
        	// btnCancel
        	// 
        	this.btnCancel.Location = new System.Drawing.Point(432, 352);
        	this.btnCancel.Name = "btnCancel";
        	this.btnCancel.Size = new System.Drawing.Size(75, 23);
        	this.btnCancel.TabIndex = 9;
        	this.btnCancel.Text = "Cancel";
        	this.btnCancel.UseVisualStyleBackColor = true;
        	this.btnCancel.Click += new System.EventHandler(this.CancelButton_Click);
        	// 
        	// SessionTimeLabel
        	// 
        	this.SessionTimeLabel.AutoSize = true;
        	this.SessionTimeLabel.Location = new System.Drawing.Point(11, 30);
        	this.SessionTimeLabel.Name = "SessionTimeLabel";
        	this.SessionTimeLabel.Size = new System.Drawing.Size(102, 13);
        	this.SessionTimeLabel.TabIndex = 10;
        	this.SessionTimeLabel.Text = "Session Time (Min.):";
        	// 
        	// BreakTimeLabel
        	// 
        	this.BreakTimeLabel.AutoSize = true;
        	this.BreakTimeLabel.Location = new System.Drawing.Point(11, 53);
        	this.BreakTimeLabel.Name = "BreakTimeLabel";
        	this.BreakTimeLabel.Size = new System.Drawing.Size(93, 13);
        	this.BreakTimeLabel.TabIndex = 11;
        	this.BreakTimeLabel.Text = "Break Time (Min.):";
        	// 
        	// groupBox1
        	// 
        	this.groupBox1.Controls.Add(this.TickingSoundTextBox);
        	this.groupBox1.Controls.Add(this.PickAlarmSoundFile);
        	this.groupBox1.Controls.Add(this.AlarmSoundTextBox);
        	this.groupBox1.Controls.Add(this.AlarmSoundFileLabel);
        	this.groupBox1.Controls.Add(this.PickTickingSoundFile);
        	this.groupBox1.Controls.Add(this.TickSoundFileLabel);
        	this.groupBox1.Controls.Add(this.PlayTickingSoundCheckBox);
        	this.groupBox1.Controls.Add(this.PlayAlarmSoundCheckBox);
        	this.groupBox1.Location = new System.Drawing.Point(12, 130);
        	this.groupBox1.Name = "groupBox1";
        	this.groupBox1.Size = new System.Drawing.Size(583, 100);
        	this.groupBox1.TabIndex = 16;
        	this.groupBox1.TabStop = false;
        	this.groupBox1.Text = "Sound Options";
        	// 
        	// groupBox2
        	// 
        	this.groupBox2.Controls.Add(this.breakTimeUpDown);
        	this.groupBox2.Controls.Add(this.sessionTimeUpDown);
        	this.groupBox2.Controls.Add(this.SessionTimeLabel);
        	this.groupBox2.Controls.Add(this.BreakTimeLabel);
        	this.groupBox2.Location = new System.Drawing.Point(12, 12);
        	this.groupBox2.Name = "groupBox2";
        	this.groupBox2.Size = new System.Drawing.Size(583, 100);
        	this.groupBox2.TabIndex = 17;
        	this.groupBox2.TabStop = false;
        	this.groupBox2.Text = "Timing Options";
        	// 
        	// breakTimeUpDown
        	// 
        	this.breakTimeUpDown.Location = new System.Drawing.Point(116, 51);
        	this.breakTimeUpDown.Maximum = new decimal(new int[] {
        	        	        	60,
        	        	        	0,
        	        	        	0,
        	        	        	0});
        	this.breakTimeUpDown.Minimum = new decimal(new int[] {
        	        	        	1,
        	        	        	0,
        	        	        	0,
        	        	        	0});
        	this.breakTimeUpDown.Name = "breakTimeUpDown";
        	this.breakTimeUpDown.Size = new System.Drawing.Size(57, 20);
        	this.breakTimeUpDown.TabIndex = 16;
        	this.breakTimeUpDown.Value = new decimal(new int[] {
        	        	        	5,
        	        	        	0,
        	        	        	0,
        	        	        	0});
        	// 
        	// sessionTimeUpDown
        	// 
        	this.sessionTimeUpDown.Location = new System.Drawing.Point(116, 30);
        	this.sessionTimeUpDown.Maximum = new decimal(new int[] {
        	        	        	60,
        	        	        	0,
        	        	        	0,
        	        	        	0});
        	this.sessionTimeUpDown.Minimum = new decimal(new int[] {
        	        	        	1,
        	        	        	0,
        	        	        	0,
        	        	        	0});
        	this.sessionTimeUpDown.Name = "sessionTimeUpDown";
        	this.sessionTimeUpDown.Size = new System.Drawing.Size(57, 20);
        	this.sessionTimeUpDown.TabIndex = 15;
        	this.sessionTimeUpDown.Value = new decimal(new int[] {
        	        	        	25,
        	        	        	0,
        	        	        	0,
        	        	        	0});
        	// 
        	// groupBox3
        	// 
        	this.groupBox3.Location = new System.Drawing.Point(12, 246);
        	this.groupBox3.Name = "groupBox3";
        	this.groupBox3.Size = new System.Drawing.Size(583, 100);
        	this.groupBox3.TabIndex = 18;
        	this.groupBox3.TabStop = false;
        	this.groupBox3.Text = "Log Output Format Options";
        	// 
        	// Options
        	// 
        	this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
        	this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
        	this.ClientSize = new System.Drawing.Size(608, 387);
        	this.Controls.Add(this.groupBox3);
        	this.Controls.Add(this.groupBox2);
        	this.Controls.Add(this.groupBox1);
        	this.Controls.Add(this.btnCancel);
        	this.Controls.Add(this.btnSave);
        	this.Name = "Options";
        	this.Text = "Options";
        	this.Load += new System.EventHandler(this.Options_Load);
        	this.groupBox1.ResumeLayout(false);
        	this.groupBox1.PerformLayout();
        	this.groupBox2.ResumeLayout(false);
        	this.groupBox2.PerformLayout();
        	((System.ComponentModel.ISupportInitialize)(this.breakTimeUpDown)).EndInit();
        	((System.ComponentModel.ISupportInitialize)(this.sessionTimeUpDown)).EndInit();
        	this.ResumeLayout(false);
        }
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.NumericUpDown sessionTimeUpDown;
        private System.Windows.Forms.NumericUpDown breakTimeUpDown;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox1;

        #endregion

        private System.Windows.Forms.Label TickSoundFileLabel;
        private System.Windows.Forms.Label AlarmSoundFileLabel;
        private System.Windows.Forms.TextBox TickingSoundTextBox;
        private System.Windows.Forms.Button PickTickingSoundFile;
        private System.Windows.Forms.Button PickAlarmSoundFile;
        private System.Windows.Forms.TextBox AlarmSoundTextBox;
        private System.Windows.Forms.CheckBox PlayTickingSoundCheckBox;
        private System.Windows.Forms.CheckBox PlayAlarmSoundCheckBox;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Label SessionTimeLabel;
        private System.Windows.Forms.Label BreakTimeLabel;
    }
}